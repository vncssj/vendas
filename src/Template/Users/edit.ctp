<div class="col-12">
    <?= $this->Form->create($user) ?>
    <div class="clearfix">&nbsp</div>
    <fieldset>
        <legend><?= __('Editar Usuário') ?></legend>
        <div class="clearfix">&nbsp</div>
        <?php
        echo $this->Form->control('login');
        echo $this->Form->control('password');
        echo $this->Form->control('email');
        echo $this->Form->control('imagem');
        ?>
    </fieldset>
    <div class="clearfix">&nbsp</div>
    <div class="col-12 centralizar">
        <?= $this->Form->button('Salvar', ['class' => 'btn btn-info']) ?>
    </div>
    <div class="clearfix">&nbsp</div>
    <?= $this->Form->end() ?>
</div>