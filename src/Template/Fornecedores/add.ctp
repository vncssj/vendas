<div class="clearfix">&nbsp</div>
<div class="row">
    <div class="col-md-4"><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fas fa-arrow-left']).' Voltar', ['action' => 'index'], ['escape'=> false,'class' => 'btn btn-sm btn-light'])?></div>
    <div class="col-md-4"><h3>Novo fornecedor</h3></div>
</div>
<div class="col-xs-12">
    <?= $this->Form->create($fornecedore) ?>
    <fieldset>
        <?php
            echo $this->Form->control('nome');
            echo $this->Form->control('razao_social');
            echo $this->Form->control('cnpj');
            echo $this->Form->control('inscricao_estadual');
            echo $this->Form->control('endereco');
            echo $this->Form->control('telefone');
            echo $this->Form->control('email');
            echo $this->Form->control('observacoes');
        ?>
    </fieldset>
    <?= $this->Form->button('Concluir', ['class' => 'btn btn-sm btn-info col-md-2 offset-md-5']) ?>
    <div class="clearfix">&nbsp</div>
    <?= $this->Form->end() ?>
</div>
