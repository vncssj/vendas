<div class="col-xs-12">
    <h3><?= h($produto->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nome') ?></th>
            <td><?= h($produto->nome) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($produto->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Preco Custo') ?></th>
            <td><?= $this->Number->format($produto->preco_custo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Preco Venda') ?></th>
            <td><?= $this->Number->format($produto->preco_venda) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lucro') ?></th>
            <td><?= $this->Number->format($produto->lucro) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Quantidade') ?></th>
            <td><?= $this->Number->format($produto->quantidade) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descricao') ?></h4>
        <?= $this->Text->autoParagraph(h($produto->descricao)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Venda Produtos') ?></h4>
        <?php if (!empty($produto->venda_produtos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Venda Id') ?></th>
                <th scope="col"><?= __('Produto Id') ?></th>
                <th scope="col"><?= __('Quantidade') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($produto->venda_produtos as $vendaProdutos): ?>
            <tr>
                <td><?= h($vendaProdutos->id) ?></td>
                <td><?= h($vendaProdutos->venda_id) ?></td>
                <td><?= h($vendaProdutos->produto_id) ?></td>
                <td><?= h($vendaProdutos->quantidade) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'VendaProdutos', 'action' => 'view', $vendaProdutos->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'VendaProdutos', 'action' => 'edit', $vendaProdutos->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'VendaProdutos', 'action' => 'delete', $vendaProdutos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $vendaProdutos->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
