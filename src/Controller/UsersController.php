<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function beforeFilter(\Cake\Event\Event $event)
    {
        //  $this->Auth->allow(['add']);
    }

    public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $dados = $this->request->data;
            $time = Time::now();
            $imagem = substr($dados['imagem']['name'], 0, -4) . $time->day . $time->month . $time->year;
            $temp = $dados['imagem']['tmp_name'];
            $tipo = $dados['imagem']['type'];
            $ext = substr($dados['imagem']['name'], -3);
            $caminho = "../webroot/img/users/";
            $tipos_permitidos = ['image/jpg', 'image/jpeg', 'image/png'];
            $dados['imagem'] = $imagem . "." . $ext;
            $user = $this->Users->patchEntity($user, $dados);
            if ($this->Users->save($user)) {
                if (in_array($tipo, $tipos_permitidos)) {
                    if (is_uploaded_file($temp)) {
                        move_uploaded_file($temp, $caminho . $imagem . $ext);
                        $this->Flash->success('Novo Administrador adicionado.');
                        return $this->redirect(['action' => 'index']);
                    }
                }
            }
            $this->Flash->error('O Administrado não foi criado, tente novamente.');
        }
        $this->set(compact('user'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect(['controller' => 'Pages', 'action' => 'home']);
                //                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error('Login ou Senha incorreto');
        }
        $this->viewBuilder()->setLayout('externo');
    }

    public function logout()
    {
        $this->Flash->success('Você saiu.');
        return $this->redirect($this->Auth->logout());
    }
}
