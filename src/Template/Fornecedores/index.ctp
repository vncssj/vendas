<div class="table-title p-3 rounded-top shadow">
                	
	<div class="row">
		<div class="col-7 pl-4">
			<h2>Administração de <b>Fornecedores</b></h2>
		</div>
			
		<div class="col-2">
		</div>
			
		<div class="col-3 pr-4">
			<?= $this->Html->link('Adicionar', ['action' => 'add'], ['class' => 'btn btn-success float-right shadow-sm'])?>
		   
			<a href="#" class="btn btn-danger float-right shadow-sm mr-2" data-target="#modalMult_Deletar" data-toggle="modal"><i class="fas fa-trash-alt"></i></a>
		</div>
	</div>
		
</div>
              
<div class="crud px-3 bg-white rounded-bottom shadow">
    <div class="row">
        <div class="col-12">
            <table class="table bg-white table-borderless mt-2 table-hover table-striped table-responsive-lg table-responsive-md">
                <thead>
                    <tr>
						<th scope="col">
							<span class="custom-checkbox">
								<input type="checkbox" id="selectAll">
								<label for="selectAll"></label>
							</span>
						</th>
                        <th scope="col" class="align-middle"><?= $this->Paginator->sort('NOME') ?></th>
						<th scope="col" class="align-middle"><?= $this->Paginator->sort('RAZAO SOCIAL') ?></th>
						<th scope="col" class="align-middle"><?= $this->Paginator->sort('TELEFONE') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($fornecedores as $fornecedore): ?>
                        <tr>
                            <td>
								<span class="custom-checkbox">
									<input type="checkbox" name="options[]" value="<?= $this->Number->format($fornecedore->id) ?>">
									<label for="checkbox"></label>
								</span>
							</td>
							<td class="align-middle"><?= h($fornecedore->nome) ?></td>
							<td class="align-middle"><?= h($fornecedore->razao_social) ?></td>
							<td class="align-middle"><?= h($fornecedore->telefone) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('Ver'), ['action' => 'view', $fornecedore->id]) ?>
                    			<?= $this->Html->link(__('Editar'), ['action' => 'edit', $fornecedore->id]) ?>
                    			<?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $fornecedore->id], ['confirm' => __('Are you sure you want to delete # {0}?', $fornecedore->id)]) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div>
        </div>
	</div>
</div>



<script>
<!-- funcao select all chechbox -->
	
	$(document).ready(function(){
	// Activate tooltip
		$('[data-toggle="tooltip"]').tooltip();
		
		// Select/Deselect checkboxes
		var checkbox = $('table tbody input[type="checkbox"]');
		$("#selectAll").click(function(){
			if(this.checked){
				checkbox.each(function(){
					this.checked = true;                        
				});
			} else{
				checkbox.each(function(){
					this.checked = false;                        
				});
			} 
		});
		checkbox.click(function(){
			if(!this.checked){
				$("#selectAll").prop("checked", false);
			}
		});
	});
</script>