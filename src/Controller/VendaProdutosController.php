<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * VendaProdutos Controller
 *
 * @property \App\Model\Table\VendaProdutosTable $VendaProdutos
 *
 * @method \App\Model\Entity\VendaProduto[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class VendaProdutosController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Vendas', 'Produtos']
        ];
        $vendaProdutos = $this->paginate($this->VendaProdutos);

        $this->set(compact('vendaProdutos'));
    }

    /**
     * View method
     *
     * @param string|null $id Venda Produto id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $vendaProduto = $this->VendaProdutos->get($id, [
            'contain' => ['Vendas', 'Produtos']
        ]);

        $this->set('vendaProduto', $vendaProduto);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $vendaProduto = $this->VendaProdutos->newEntity();
        if ($this->request->is('post')) {
            $vendaProduto = $this->VendaProdutos->patchEntity($vendaProduto, $this->request->getData());
            if ($this->VendaProdutos->save($vendaProduto)) {
                $this->Flash->success(__('The venda produto has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The venda produto could not be saved. Please, try again.'));
        }
        $vendas = $this->VendaProdutos->Vendas->find('list', ['limit' => 200]);
        $produtos = $this->VendaProdutos->Produtos->find('list', ['limit' => 200]);
        $this->set(compact('vendaProduto', 'vendas', 'produtos'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Venda Produto id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $vendaProduto = $this->VendaProdutos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $vendaProduto = $this->VendaProdutos->patchEntity($vendaProduto, $this->request->getData());
            if ($this->VendaProdutos->save($vendaProduto)) {
                $this->Flash->success(__('The venda produto has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The venda produto could not be saved. Please, try again.'));
        }
        $vendas = $this->VendaProdutos->Vendas->find('list', ['limit' => 200]);
        $produtos = $this->VendaProdutos->Produtos->find('list', ['limit' => 200]);
        $this->set(compact('vendaProduto', 'vendas', 'produtos'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Venda Produto id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $vendaProduto = $this->VendaProdutos->get($id);
        if ($this->VendaProdutos->delete($vendaProduto)) {
            $this->Flash->success(__('The venda produto has been deleted.'));
        } else {
            $this->Flash->error(__('The venda produto could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
