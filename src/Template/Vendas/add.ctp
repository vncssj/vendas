<div class="col-12">

    <div class="clearfix">&nbsp</div>
    <div class="row">
        <div class="col-md-4">

            <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fas fa-arrow-left']) . ' Voltar', ['action' => 'index'], ['escape' => false, 'class' => 'btn btn-info btn-sm']); ?>
        </div>
        <div class="col-md-4">
            <h3>Nova Venda</h3>
        </div>

    </div>
    <div class="col-xs-12">
        <?= $this->Form->create($venda) ?>
        <fieldset>
            <?= $this->Form->control('cliente_id', ['options' => $clientes, 'empty' => 'Selecione o Cliente']); ?>
            <div class="clearfix">&nbsp</div>
            <span id="sub-titulo">Produtos</span>
            <div class="clearfix">&nbsp</div>
            <div id="produtos" style="margin-left: 30px;">
                <div class="grupo">
                    <div class="produto">
                        <?= $this->Form->control('produto[]', ['class' => 'select-produto', 'label' => false, 'options' => $produtos, 'empty' => 'Selecione o Produto']); ?>
                    </div>
                    <div class="quantidade row">
                        <div class="col-md-2 valor">
                            <?= $this->Form->control('valor', ['readonly']) ?>
                        </div>
                        <div class="col-md-2 col-xs-5 pai-quantidade">
                            <?= $this->Form->control('quantidade[]', ['class' => 'input-quantidade', 'label' => 'Quantidade', 'type' => 'number', 'min' => 1]) ?>
                        </div>
                        <div class="col-md-2 sub-total">
                            <?= $this->Form->control('sub-total', ['readonly']) ?>
                        </div>
                    </div>
                    <div class="divisor"></div>
                    <div class="clearfix">&nbsp</div>
                </div>
            </div>
            <div class="clearfix">&nbsp</div>
            <?= $this->Form->button($this->Html->tag('i', '', ['class' => 'fas fa-plus']) . ' Produto', ['id' => 'add-produto', 'type' => 'button', 'class' => 'btn btn-sm btn-primary', 'style' => 'float: right']) ?>
            <?= $this->Form->control('status', ['type' => 'checkbox', 'label' => 'Venda Concluida']); ?>
        </fieldset>
        <?= $this->Form->button('Concluir', ['class' => 'btn btn-info col-md-2 offset-md-5']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>

<script>
    $("#add-produto").click(function() {
        $.ajax({
            type: "GET",
            url: "<?= $this->request->webroot ?>vendas/getProduto",
            success: function(data){
                $("#produtos").append(data);
            }
        });
    });
    $(".deletar").click(function() {
        $(this).parent().parent().parent().remove();
    });
    $(".select-produto").change(function() {
        let select = $(this);
        $.ajax({
            type: "GET",
            url: "consultaPreco/" + $(this).val(),
            success: function(data) {
                select.parent().parent().siblings(".quantidade").children(".valor").children(".input").children("#valor").val(data);
            }
        });
        $.ajax({
            type: "GET",
            url: "consultaQuantidade/" + $(this).val(),
            success: function(data) {
                select.parent().parent().siblings(".quantidade").children(".pai-quantidade").children(".input").children("#quantidade").attr("max", data);
            }
        });
    });

    $(".input-quantidade").change(function() {
        let valor = $(this).parent().parent().siblings(".valor").children(".input").children("#valor").val();
        let quantidade = $(this).val();
        let subTotal = $(this).parent().parent().siblings(".sub-total").children(".input").children("#sub-total");
        subTotal.val(valor * quantidade);
    });
</script>