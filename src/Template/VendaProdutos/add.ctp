<div class="vendaProdutos form large-9 medium-8 columns content">
    <?= $this->Form->create($vendaProduto) ?>
    <fieldset>
        <legend><?= __('Add Venda Produto') ?></legend>
        <?php
            echo $this->Form->control('venda_id', ['options' => $vendas]);
            echo $this->Form->control('produto_id', ['options' => $produtos]);
            echo $this->Form->control('quantidade');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
