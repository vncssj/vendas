<div class="table-title p-3 rounded-top shadow">

    <div class="row">
        <div class="col-6 pl-4">
            <h2>Administração de <b>Administradores</b></h2>
        </div>

        <div class="col-2">
        </div>

        <div class="col-4 pr-4">
            <?= $this->Html->link('Adicionar', ['action' => 'add'], ['class' => 'btn btn-success float-right shadow-sm']) ?>

            <a href="#" class="btn btn-danger float-right shadow-sm mr-2" data-target="#modalMult_Deletar" data-toggle="modal"><i class="fas fa-trash-alt"></i></a>
        </div>
    </div>

</div>
<div class="crud px-3 bg-white rounded-bottom shadow">
    <div class="row">
        <div class="col-12">
            <table class="table bg-white table-borderless mt-2 table-hover table-striped table-responsive-lg table-responsive-md">
                <thead>
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('login') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('imagem') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($users as $user) : ?>
                        <tr>
                            <td><?= $this->Number->format($user->id) ?></td>
                            <td><?= h($user->login) ?></td>
                            <td><?= h($user->email) ?></td>
                            <td><?= h($user->imagem) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['action' => 'view', $user->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $user->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div>
        </div>
	</div>
</div>