<div class="table-title p-3 rounded-top shadow">

    <div class="row">
        <div class="col-6 pl-4">
            <h2>Administração de <b>Produtos</b></h2>
        </div>

        <div class="col-2">
        </div>

        <div class="col-4 pr-4">
            <?= $this->Html->link('Adicionar', ['action' => 'add'], ['class' => 'btn btn-success float-right shadow-sm']) ?>

            <a href="#" class="btn btn-danger float-right shadow-sm mr-2" data-target="#modalMult_Deletar" data-toggle="modal"><i class="fas fa-trash-alt"></i></a>
        </div>
    </div>

</div>
<div class="crud px-3 bg-white rounded-bottom shadow">
    <div class="row">

        <div class="col-12">
            <table class="table bg-white table-borderless mt-2 table-hover table-striped table-responsive-lg table-responsive-md">
                <thead>
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('nome') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('preco_custo') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('preco_venda') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('lucro') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('quantidade') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($produtos as $produto) : ?>
                        <tr>
                            <td><?= $this->Number->format($produto->id) ?></td>
                            <td><?= h($produto->nome) ?></td>
                            <td><?= $this->Number->format($produto->preco_custo) ?></td>
                            <td><?= $this->Number->format($produto->preco_venda) ?></td>
                            <td><?= $this->Number->format($produto->lucro) ?></td>
                            <td><?= $this->Number->format($produto->quantidade) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('Ver'), ['action' => 'view', $produto->id]) ?>
                                <?= $this->Html->link(__('Editar'), ['action' => 'edit', $produto->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $produto->id], ['confirm' => __('Are you sure you want to delete # {0}?', $produto->id)]) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
            </div>
        </div>
    </div>
</div>