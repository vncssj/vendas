<div class="grupo">
    <div class="produto">
        <?= $this->Form->control('produto[]', ['class' => 'select-produto', 'label' => false, 'options' => $produtos, 'empty' => 'Selecione o Produto']); ?>
    </div>
    <div class="quantidade row">
        <div class="col-md-2 valor">
            <?= $this->Form->control('valor', ['readonly']) ?>
        </div>
        <div class="col-md-2 col-xs-5 pai-quantidade">
            <?= $this->Form->control('quantidade[]', ['class' => 'input-quantidade', 'label' => 'Quantidade', 'type' => 'number', 'min' => 1, 'value' => 1]) ?>
        </div>
        <div class="col-md-2 sub-total">
            <?= $this->Form->control('sub-total', ['readonly']) ?>
        </div>
        <div class="offset-md-5 col-md-1 col-xs-5" style="display: flex; padding: 30px; justify-content: center;">
            <?= $this->Form->button('Excluir', ['class' => 'btn btn-danger deletar', 'type' => 'button']) ?>
        </div>
    </div>
    <div class="divisor"></div>
    <div class="clearfix">&nbsp</div>
</div>
<script>
    $(".deletar").click(function() {
        $(this).parent().parent().parent().remove();
    });
    $(".select-produto").change(function() {
        let select = $(this);
        $.ajax({
            type: "GET",
            url: "consultaPreco/" + $(this).val(),
            success: function(data) {
                select.parent().parent().siblings(".quantidade").children(".valor").children(".input").children("#valor").val(data);
            }
        });
        $.ajax({
            type: "GET",
            url: "consultaQuantidade/" + $(this).val(),
            success: function(data) {
                select.parent().parent().siblings(".quantidade").children(".pai-quantidade").children(".input").children("#quantidade").attr("max", data);
                console.log(data);
            }
        });
    });

    $(".input-quantidade").change(function() {
        let valor = $(this).parent().parent().siblings(".valor").children(".input").children("#valor").val();
        let quantidade = $(this).val();
        let subTotal = $(this).parent().parent().siblings(".sub-total").children(".input").children("#sub-total");
        subTotal.val(valor * quantidade);
    });

    $("#add-produto").click(function() {
        $("#produtos").append($("#conteudo").html());
    });
</script>