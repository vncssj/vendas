<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Venda Entity
 *
 * @property int $id
 * @property int $cliente_id
 * @property string|null $status
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property float|null $valor
 * @property float|null $lucro
 *
 * @property \App\Model\Entity\Cliente $cliente
 * @property \App\Model\Entity\VendaProduto[] $venda_produtos
 */
class Venda extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'cliente_id' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'valor' => true,
        'lucro' => true,
        'cliente' => true,
        'venda_produtos' => true
    ];
}
