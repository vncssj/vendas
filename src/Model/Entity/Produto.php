<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Produto Entity
 *
 * @property int $id
 * @property int $fornecedor_id
 * @property string $nome
 * @property string $descricao
 * @property float $preco_custo
 * @property float $preco_venda
 * @property float $lucro
 * @property int|null $quantidade
 *
 * @property \App\Model\Entity\VendaProduto[] $venda_produtos
 */
class Produto extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'fornecedor_id' => true,
        'nome' => true,
        'descricao' => true,
        'preco_custo' => true,
        'preco_venda' => true,
        'lucro' => true,
        'quantidade' => true,
        'venda_produtos' => true
    ];
}
