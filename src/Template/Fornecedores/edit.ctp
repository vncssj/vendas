<div class="col-12">
    <?= $this->Form->create($fornecedore) ?>
    <div class="col-xs-12">
        <div class="clearfix">&nbsp</div>
    </div>
    <fieldset>
        <legend class="centralizar"><?= __('Editar Fornecedor') ?></legend>
        <div class="clearfix">&nbsp</div>
        <?php
            echo $this->Form->control('nome');
            echo $this->Form->control('razao_social');
            echo $this->Form->control('cnpj');
            echo $this->Form->control('inscricao_estadual');
            echo $this->Form->control('endereco');
            echo $this->Form->control('telefone');
            echo $this->Form->control('email');
            echo $this->Form->control('observacoes');
        ?>
    </fieldset>
    <div class="clearfix">&nbsp</div>
    <div class="col-xs-12 centralizar">
        <?= $this->Form->button('Salvar', ['class' => 'btn btn-info']) ?>
    </div>
    <div class="clearfix">&nbsp</div>
    <?= $this->Form->end() ?>
</div>
