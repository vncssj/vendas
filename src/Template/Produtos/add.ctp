<div class="container">
    <div class="clearfix">&nbsp</div>
    <div class="row">
        <div class="col-md-4">
            <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fas fa-arrow-left']) .  ' Voltar', ['action' => 'index'], ['escape' => false, 'class' => 'btn btn-info btn-sm']) ?>
        </div>
        <div class="col-md-4 centralizar">
            <h3>Novo Produto</h3>
        </div>
    </div>
    <div class="clearfix">&nbsp</div>
    <div class="col-md-12">
        <?= $this->Form->create($produto) ?>
        <fieldset class="form-row">
            <div class="col-md-6">
                <?= $this->Form->control('nome'); ?>
            </div>
            <div class="col-md-6">
                <?= $this->Form->control('fornecedores_id', ['name' => 'fornecedor_id']); ?>
            </div>
            <div class="col-xs-12 col-md-4">
                <?= $this->Form->control('preco_custo'); ?>
            </div>
            <div class="col-xs-12 col-md-4">
                <?= $this->Form->control('preco_venda'); ?>
            </div>
            <div class="col-xs-12 col-md-4">
                <?= $this->Form->control('quantidade'); ?>
            </div>
            <div class="col-xs-12 col-md-12">
                <?= $this->Form->control('descricao'); ?>
            </div>
        </fieldset>
        <div class="clearfix">&nbsp</div>
        <?= $this->Form->button('Concluir', ['class' => 'btn btn-info col-md-2 offset-md-5']) ?>
        <div class="clearfix">&nbsp</div>
        <?= $this->Form->end() ?>
    </div>
</div>