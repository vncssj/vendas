<div class="col-md-4 offset-md-4">
    <h1>Login</h1>
    <?= $this->Form->create() ?>
    <?= $this->Form->control('login') ?>
    <?= $this->Form->control('password', ['label' => 'Senha']) ?>
    <?= $this->Form->button('Entrar', ['class' => 'btn btn-info ccol-md-2 offset-md-5']) ?>
    <?= $this->Form->end() ?>
    <div class="clearfix">&nbsp</div>
</div>