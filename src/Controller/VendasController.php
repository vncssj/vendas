<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;

/**
 * Vendas Controller
 *
 * @property \App\Model\Table\VendasTable $Vendas
 *
 * @method \App\Model\Entity\Venda[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class VendasController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Clientes'],
            'conditions' => ['status' => 1]
        ];
        $vendas = $this->paginate($this->Vendas);

        $this->set(compact('vendas'));
    }

    public function cotacoes()
    {
        $this->paginate = [
            'contain' => ['Clientes'],
            'conditions' => ['status <>' => 1]
        ];
        $vendas = $this->paginate($this->Vendas);

        $this->set(compact('vendas'));
    }

    /**
     * View method
     *
     * @param string|null $id Venda id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $venda = $this->Vendas->get($id, [
            'contain' => ['Clientes', 'VendaProdutos']
        ]);

        $this->set('venda', $venda);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $venda = $this->Vendas->newEntity();
        if ($this->request->is('post')) {
            $dados = $this->request->data;
            $produtos = $dados['produto'];
            $quantidades = $dados['quantidade'];
            $valor_total = 0;
            $lucro_total = 0;
            $this->loadModel("Produtos");
            foreach ($produtos as $indice => $produto) {
                if ($produto != "") {
                    $get_produto = $this->Produtos->get($produto);
                    $valor_produto = $get_produto->preco_venda;
                    $sub_total = $valor_produto * $quantidades[$indice];
                    $valor_total += $sub_total;
                    $lucro_total += $get_produto->lucro * $quantidades[$indice];
                }
            }
            $venda = $this->Vendas->patchEntity($venda, $this->request->getData());
            $venda->valor = $valor_total;
            $venda->lucro = $lucro_total;
            if ($this->Vendas->save($venda)) {
                $this->Flash->success(__('Venda/Cotação salva com sucesso!'));
                foreach ($produtos as $indice => $produto) {
                    if ($produto != "") {
                        //Lucro sobre cada PRODUTO DA VENDA
                        $get_produto = $this->Produtos->get($produto);
                        $valor_produto = $get_produto->preco_venda;
                        $lucro_produto = $get_produto->lucro;
                        $sub_total = $valor_produto * $quantidades[$indice];
                        $sub_lucro = $lucro_produto * $quantidades[$indice];

                        $this->loadModel("VendaProdutos");
                        $vendaProdutos = $this->VendaProdutos->newEntity();
                        $vendaProdutos->venda_id = $venda->id;
                        $vendaProdutos->produto_id = $produto;
                        $vendaProdutos->quantidade = $quantidades[$indice];
                        $vendaProdutos->valor = $sub_total;
                        $vendaProdutos->lucro = $sub_lucro;
                        $this->VendaProdutos->save($vendaProdutos);

                        //DIMINUINDO QUANTIDADE DE PRODUTOS CASO DEJA UMA VENDA
                        $get_produto->quantidade -=  $quantidades[$indice];
                        $this->Produtos->save($get_produto);
                    }
                }
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The venda could not be saved. Please, try again.'));
        }
        $clientes = $this->Vendas->Clientes->find('list', ['valueField' => 'nome']);

        $this->loadModel('Produtos');
        $produtos = $this->Produtos->find('list', ['valueField' => 'nome']);
        $this->set(compact('venda', 'clientes', 'produtos'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Venda id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $venda = $this->Vendas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $venda = $this->Vendas->patchEntity($venda, $this->request->getData());
            if ($this->Vendas->save($venda)) {
                $this->Flash->success(__('The venda has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The venda could not be saved. Please, try again.'));
        }
        $clientes = $this->Vendas->Clientes->find('all', ['valueField' => 'nome']);
        $this->loadModel('Produtos');
        $produtos = $this->Produtos->find('list', ['valueField' => 'nome']);
        $this->set(compact('venda', 'clientes', 'produtos'));
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Venda id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $venda = $this->Vendas->get($id);
        if ($this->Vendas->delete($venda)) {
            $this->Flash->success(__('The venda has been deleted.'));
        } else {
            $this->Flash->error(__('The venda could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function consultaPreco($id = null)
    {
        $this->loadModel('Produtos');
        $produto = $this->Produtos->get($id);
        echo $produto['preco_venda'];
        die;
    }

    public function consultaQuantidade($id = null)
    {
        $this->loadModel('Produtos');
        $produto = $this->Produtos->get($id);
        echo $produto['quantidade'];
        die;
    }

    public function financas()
    {
        $vendas = $this->Vendas->find('all')->where(['status' => 1]);
        $cotacoes = $this->Vendas->find('all')->where(['status' => 0]);

        // Get the current time.
        $now = Time::now();
        $mes_atual = $now->month;
        $vendas_mensais = [];
        $cotacoes_mensais = [];
        $ids_vendas = [];
        $ids_cotacoes = [];

        //Get vendas mensais
        foreach ($vendas as $venda) {
            $mes_compra = $venda->created->month;
            if ($mes_compra == $mes_atual) {
                $vendas_mensais[] = $venda;
            }
        }

        //Get cotações mensais
        foreach ($cotacoes as $cotacao) {
            $mes_compra = $cotacao->created->month;
            if ($mes_compra == $mes_atual) {
                $cotacoes_mensais[] = $cotacao;
            }
        }

        //Get ID das Vendas
        foreach ($vendas as $venda) {
            $ids_vendas[] = $venda->id;
        }

        //Get ID das cotações
        foreach ($cotacoes as $cotacao) {
            $ids_cotacoes[] = $cotacao->id;
        }

        if(count($ids_vendas) > 0){
            $vendas_rank = $this->Vendas->VendaProdutos
            ->find('all', ['fields' => ['Produtos.nome', 'soma' => 'SUM(VendaProdutos.quantidade)']])
            ->contain('Produtos')
            ->where(['venda_id IN' => $ids_vendas])
            ->group('produto_id')
            ->order(['soma' => 'DESC'])
            ->limit(5);
        }else{
            $vendas_rank = 0;
        }
            
        $vendas = $vendas->count();
        $cotacoes = $cotacoes->count();
        $vendas_mensais = count($vendas_mensais);
        $cotacoes_mensais = count($cotacoes_mensais);
        $this->set(compact('cotacoes', 'vendas', 'cotacoes_mensais', 'vendas_mensais','vendas_rank'));
    }

    public function getProduto(){
        $this->loadModel('Produtos');
        $produtos = $this->Produtos->find('list', ['valueField' => 'nome'])->where(['quantidade >' => 0]);
        $this->set(compact('produtos'));
    }
}
