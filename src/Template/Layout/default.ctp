<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'SGV - Sistema de Gerenciamento de Vendas';
//$controller = $this->request->controller;
$controller = $this->request->getParam('controller');
$action = $this->request->getParam('action');
$sessao = $this->request->getSession();
$imagem = $this->getRequest()->getSession()->read('Auth.User.imagem');
?>
<!DOCTYPE html>
<html>

<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <script src="https://kit.fontawesome.com/a048c6ba8a.js"></script>
    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('bootstrap.css') ?>
    <?= $this->Html->css('estilo.css') ?>
    <?= $this->Html->css('style.css') ?>
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    <?= $this->Html->script('bootstrap'); ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>

<body>

    <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <nav class="navbar navbar-vertical navbar-expand">

                <a class="navbar-brand text-center" href="#"><i class="fas fa-cube"></i> PANEL</a>

                <a class="navbar-main">
                    <span class="title">Navegação</span>
                </a>

                <div class="collapse navbar-collapse" id="navbarVerticalCollapse">
                    <ul class="sidebar-nav flex-column">

                        <li class="nav-item menu-maior">
                            <a class="navbar-link links" id="grupo_dashboard" href="#dashboard" data-toggle="collapse" aria-expanded="false" aria-controls="dashboard">
                                <i class="fas fa-tachometer-alt"></i>
                                <span class="title">Dashboard</span>
                            </a>

                            <ul class="nav collapse submenu text-light m-0" id="dashboard" data-parent="#navbarVerticalCollapse">
                                <li class="nav-item" data-local="financas"><?= $this->Html->link('Financeiro', ['controller' => 'Financeiro', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
                            </ul>
                        </li>

                        <li class="nav-item menu-maior">
                            <a class="navbar-link links" id="grupo_estoque" href="#estoque" data-toggle="collapse" aria-expanded="true" aria-controls="dashboard">
                                <i class="fas fa-shopping-cart"></i>
                                <span class="title">Estoque</span>
                            </a>

                            <ul class="nav collapse submenu text-light m-0" id="estoque" data-parent="#navbarVerticalCollapse">
                                <li class="nav-item" data-local='Produtos'><?= $this->Html->link('Produtos', ['controller' => 'Produtos', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
                                <li class="nav-item" data-local='index'><?= $this->Html->link('Vendas', ['controller' => 'Vendas', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
                                <li class="nav-item" data-local='cotacoes'><?= $this->Html->link('Cotações', "/cotacoes", ['class' => 'nav-link']) ?></li>
                            </ul>
                        </li>

                        <li class="nav-item menu-maior">
                            <a class="navbar-link links" id="grupo_cadastro" href="#cadastro" data-toggle="collapse" aria-expanded="false" aria-controls="dashboard">
                                <i class="far fa-user"></i>
                                <span class="title">Cadastro</span>
                            </a>

                            <ul class="nav collapse submenu text-light m-0" id="cadastro" data-parent="#navbarVerticalCollapse">
                                <li class="nav-item" data-local='Clientes'><?= $this->Html->link('Clientes', ['controller' => 'clientes', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
                                <li class="nav-item" data-local='Fornecedores'><?= $this->Html->link('Fornecedores', ['controller' => 'Fornecedores', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
                                <li class="nav-item" data-local='Users'><?= $this->Html->link('Administradores', ['controller' => 'Users', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
                            </ul>
                        </li>


                    </ul>
                </div>

            </nav>
        </div>
        <!-- /#sidebar-wrapper -->
        <!-- Page Content -->
        <div id="page-content-wrapper">

            <!-- Cabecalho -->
            <nav class="navbar navbar-expand-lg bg-white border-bottom shadow-sm">
                <div class="container-fluid">

                    <a href="#menu-toggle" class="btn btn-default pl-0" id="menu-toggle"><i class="fas fa-align-left"></i></a>

                    <button class="navbar-toggler text-dark" type="button" data-toggle="collapse" data-target="#navbar-top" aria-controls="navbarNav" aria-expanded="false" aria-label="Alterna navegação">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <!-- Menu Superior -->
                    <div class="collapse navbar-collapse p-0" id="navbar-top">

                        <ul class="navbar-nav ml-auto">

                            <li class="nav">
                                <a class="nav-link" href="#"><img class="img-thumbnail border-primary" style="width:45px; height:45px;" src="img/users/<?= $imagem ?>" /></a>
                            </li>

                            <li class="nav">
                                <?= $this->Html->link('Sair', ['controller' => 'Users', 'action' => 'logout'], ['class' => 'nav-link text-info']) ?>
                            </li>

                        </ul>
                    </div>
                    <!-- /#Menu Superior -->
                </div>
            </nav>
            <!-- /#Cabecalho -->
            <div class="container-fluid p-0 m-0">
                <div class="row p-4 m-0">
                    <div class="col-12 p-0 m-0 bg-white border-0" id="teste">
                        <?= $this->Flash->render() ?>
                        <?= $this->fetch('content') ?>

                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->
</body>

</html>
<script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })

    $(".nav-item").each(function() {
        let local = $(this).attr('data-local');
        if ('<?= $controller ?>' == 'Vendas') {
            if (local == '<?= $action ?>') {
                $(this).addClass("active");
                console.log('action: <?= $action ?>');
                console.log('local: ' + local);
            }
        } else {
                console.log('controller: <?= $controller ?>');
            if (local == '<?= $controller ?>') {
                $(this).addClass("active");
            }
        }
    });

    $(".menu-maior").each(function(){
        if('<? $controller ?>' == 'Vendas' && '<? $action ?>' == 'financas'){
            $("#dashboard").addClass("show");
        }else if('<? $controller ?>' == 'Vendas' && '<? $action ?>' != 'financas'){
            $("#estoque").addClass("show");
        }else{
            $("#cadastro").addClass("show");
        }
    });
</script>