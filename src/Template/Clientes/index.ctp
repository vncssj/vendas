<div class="table-title p-3 rounded-top shadow">
                	
	<div class="row">
		<div class="col-6 pl-4">
			<h2>Administração de <b>Clientes</b></h2>
		</div>
			
		<div class="col-2">
		</div>
			
		<div class="col-4 pr-4">
			<?= $this->Html->link('Adicionar', ['action' => 'add'], ['class' => 'btn btn-success float-right shadow-sm'])?>
		   
			<a href="#" class="btn btn-danger float-right shadow-sm mr-2" data-target="#modalMult_Deletar" data-toggle="modal"><i class="fas fa-trash-alt"></i></a>
		</div>
	</div>
		
</div>
<div class="crud px-3 bg-white rounded-bottom shadow">
    <div class="row">
        <div class="col-12">
            <table class="table bg-white table-borderless mt-2 table-hover table-striped table-responsive-lg table-responsive-md">
                <thead>
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('nome') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('razao_social') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('telefone') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($clientes as $cliente) : ?>
                        <tr>
                            <td><?= h($cliente->nome) ?></td>
                            <td><?= h($cliente->razao_social) ?></td>
                            <td><?= h($cliente->telefone) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('Ver'), ['action' => 'view', $cliente->id]) ?>
                                <?= $this->Html->link(__('Editar'), ['action' => 'edit', $cliente->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $cliente->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cliente->id)]) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('primeiro')) ?>
                    <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('próximo') . ' >') ?>
                    <?= $this->Paginator->last(__('ultimo') . ' >>') ?>
                </ul>
            </div>
        </div>
    </div>
</div>