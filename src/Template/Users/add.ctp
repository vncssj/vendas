<div class="col-md-12">
    <?= $this->Form->create($user, ['enctype' => 'multipart/form-data']) ?>
    <?= $this->Html->image('cake.png')?>
    <fieldset>
        <legend>Novo Admin</legend>
        <?php
            echo $this->Form->control('login');
            echo $this->Form->control('password');
            echo $this->Form->control('email');
            echo $this->Form->control('imagem' , ['type' => 'file', 'accept' => 'image/*'])
        ?>
    </fieldset>
    <?= $this->Form->button('Concluir', ['class' => 'col-m-2 offset-md-5 btn btn-info']) ?>
    <?= $this->Form->end() ?>
    <div class="cleafix">&nbsp</div>
</div>
