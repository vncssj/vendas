<div class="clearfix">&nbsp</div>
<div class="row">
    <div class="col-md-4"><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fas fa-arrow-left']).' Voltar', ['action' => 'index'], ['escape' => false,'class' => 'btn btn-light'])?></div>
    <div class="col-md-4 centralizar"><h3>Novo Cliente</h3></div>
</div>
<div class="col-xs-12">
    <?= $this->Form->create($cliente) ?>
    <fieldset>
        <?php
            echo $this->Form->control('nome');
            echo $this->Form->control('razao_social');
            echo $this->Form->control('documento');
            echo $this->Form->control('inscricao_estadual');
            echo $this->Form->control('endereco');
            echo $this->Form->control('telefone');
            echo $this->Form->control('email');
            echo $this->Form->control('observacoes');
        ?>
    </fieldset>
    <?= $this->Form->button('Concluir', ['class' => 'btn btn-info col-md-2 offset-md-4']) ?>
    <?= $this->Form->end() ?>
</div>
