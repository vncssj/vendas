<div class="col-xs-12">
    <h3>Venda/Cotação <?= h($venda->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($venda->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cliente') ?></th>
            <td><?= $venda->has('cliente') ? $this->Html->link($venda->cliente->nome, ['controller' => 'Clientes', 'action' => 'view', $venda->cliente->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= h($venda->status) ? "Venda" : "Cotação" ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Realizada') ?></th>
            <td><?= h($venda->created) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Venda Produtos') ?></h4>
        <?php if (!empty($venda->venda_produtos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Venda Id') ?></th>
                <th scope="col"><?= __('Produto Id') ?></th>
                <th scope="col"><?= __('Quantidade') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($venda->venda_produtos as $vendaProdutos): ?>
            <tr>
                <td><?= h($vendaProdutos->id) ?></td>
                <td><?= h($vendaProdutos->venda_id) ?></td>
                <td><?= h($vendaProdutos->produto_id) ?></td>
                <td><?= h($vendaProdutos->quantidade) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['controller' => 'VendaProdutos', 'action' => 'view', $vendaProdutos->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['controller' => 'VendaProdutos', 'action' => 'edit', $vendaProdutos->id]) ?>
                    <?php //$this->Form->postLink(__('Delete'), ['controller' => 'VendaProdutos', 'action' => 'delete', $vendaProdutos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $vendaProdutos->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
