<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VendaProdutosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VendaProdutosTable Test Case
 */
class VendaProdutosTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\VendaProdutosTable
     */
    public $VendaProdutos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.VendaProdutos',
        'app.Vendas',
        'app.Produtos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('VendaProdutos') ? [] : ['className' => VendaProdutosTable::class];
        $this->VendaProdutos = TableRegistry::getTableLocator()->get('VendaProdutos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->VendaProdutos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
