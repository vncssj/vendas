<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\VendaProduto[]|\Cake\Collection\CollectionInterface $vendaProdutos
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Venda Produto'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Vendas'), ['controller' => 'Vendas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Venda'), ['controller' => 'Vendas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Produtos'), ['controller' => 'Produtos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Produto'), ['controller' => 'Produtos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="vendaProdutos index large-9 medium-8 columns content">
    <h3><?= __('Venda Produtos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('venda_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('produto_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('quantidade') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($vendaProdutos as $vendaProduto): ?>
            <tr>
                <td><?= $this->Number->format($vendaProduto->id) ?></td>
                <td><?= $vendaProduto->has('venda') ? $this->Html->link($vendaProduto->venda->id, ['controller' => 'Vendas', 'action' => 'view', $vendaProduto->venda->id]) : '' ?></td>
                <td><?= $vendaProduto->has('produto') ? $this->Html->link($vendaProduto->produto->id, ['controller' => 'Produtos', 'action' => 'view', $vendaProduto->produto->id]) : '' ?></td>
                <td><?= $this->Number->format($vendaProduto->quantidade) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $vendaProduto->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $vendaProduto->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $vendaProduto->id], ['confirm' => __('Are you sure you want to delete # {0}?', $vendaProduto->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
