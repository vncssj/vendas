<style>
    .indices {
        border-width: 1px;
        border-style: solid;
        border-radius: 5px;
        color: #FFF;
        padding: 5px 20px;
        min-width: 150px;
    }

    .indices h3 {
        color: #FFF;
    }

    .topo-indices {
        display: flex;
        padding: 20px;
        justify-content: space-between;
        align-items: center;
        clear: both;
    }

    #vendas {
        background-color: #47cc59;
        border-color: #11a82d;
    }

    #cotacoes {
        background-color: #f0e152;
        border-color: #ecd618;
    }

    #vmensais {
        background-color: #2784e1;
        border-color: #101aa4;
    }

    #cmensais {
        background-color: #9a2eb5;
        border-color: #5e1479;
    }
</style>
<div class="container topo-indices">
    <div class="indices centralizar" id="vendas">
        <span>Vendas Totais</span>
        <h3><?= $vendas ?></h3>
    </div>
    <div class="indices centralizar" id="cotacoes">
        <span>Cotacões Totais</span>
        <h3><?= $cotacoes ?></h3>
    </div>
    <div class="indices centralizar" id="vmensais">
        <span>Vendas Mensais</span>
        <h3><?= $vendas_mensais ?></h3>
    </div>
    <div class="indices centralizar" id="cmensais">
        <span>Cotacões Mensais</span>
        <h3><?= $cotacoes_mensais ?></h3>
    </div>
</div>
<div class="container">
    <div class="col-3 offset-9">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Mais Vendidos</th>
                </tr>
            </thead>
            <?php if ($vendas_rank > 0) : ?>
            <?php foreach ($vendas_rank as $rank) : ?>
            <tr>
                <td><?= $rank->produto->nome ?></td>
            </tr>
            <?php endforeach; ?>
            <?php else : ?>
            <tr>
                <td><?= "Ainda não foram vendidos produtos." ?></td>
            </tr>
            <?php endif; ?>
        </table>
    </div>
</div>